FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/aws-cloudwatch-logger

WORKDIR /home/node/aws-cloudwatch-logger

RUN npm i --production

CMD ["node", "app.js"]
