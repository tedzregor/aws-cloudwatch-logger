/* global describe, it, after, before */
'use strict'

const should = require('should')

const amqp = require('amqplib')

let _app = null
let _channel = null
let _conn = null

describe('AWS-Cloudwatch Logger', () => {

  // process.env.PORT = 8081
  process.env.ACCOUNT = 'accpintID'
  process.env.INPUT_PIPE = 'demo.logger'
  process.env.BROKER = 'amqp://guest:guest@localhost'
  process.env.CONFIG = '{"region":"ap-southeast-2", "accessKeyId":"AKIA36NCHFP7VGSOC45I", "secretAccessKey":"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", "logGroupName":"Reekoh_Logger", "logStreamName":"Ted_Test2"}'

  before('init', function () {
    amqp.connect(process.env.BROKER)
    .then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
    _channel = channel
  }).catch((err) => {
    console.log(err)
  })
  })

  after('terminate child process', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(8000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#log', function () {
    it('should log data', function (done) {
      this.timeout(15000)
      /*
      let dummyData = {message: 'TEST_DATA from ESRI Service 3',
                      timeStamp: 'October 13, 2014 11:13:00'}
      */

      /*
      let dummyData = {
        title: 'Data Received.',
        device: 'ted-device',
        data: {
          message: 'Hello Message 2222222'
        }
      }
      */
     let dummyData = {
        "title": "Road Assessment API Stream - Data Received",
        "data": {
          "defect_id": "2447be00-c97b-11e9-a32f-2a2ae2dbcce4",
          "device_id": "e4e48dcd-1352-4d41-8702-2c88fe342e3d",
          "defect_type": "Cracking",
          "severity": "3",
          "imagery": "http://139.162.18.33/40ba8cb8-fd7c-4f81-b51b-79b5d7e8ded1/defects/2447be00-c97b-11e9-a32f-2a2ae2dbcce4.png",
          "latitude": "-27.105497",
          "longitude": "152.94603",
          "change": "new",
          "detected": "20190827155016",
          "last_seen": "20190827155016",
          "passed": "1",
          "missed": "0",
          "rkhDeviceInfo": {
            "_id": "MBRC-GATEWAY-DEVICE",
            "name": "MBRC-GATEWAY-DEVICE",
            "location": [
              0,
              0
            ],
            "metadata": {}
          }
        }
      }

      _channel.sendToQueue('demo.logger', new Buffer(JSON.stringify(dummyData)))
      setTimeout(done, 5000)
    })
  })
})

