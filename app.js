'use strict'
/* eslint-disable new-cap */

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Logger()
const rkhLogger = new reekoh.logger('aws-cloudwatch-logger')

const BPromise = require('bluebird')
const safeParse = BPromise.method(JSON.parse)

const AWS = require('aws-sdk')

plugin.on('log', (logs) => {
  safeParse(logs).then((parsedLogs) => {
    plugin.log({
      title: 'Data Received.',
      data: parsedLogs
    })
    const cloudwatchlogs = new AWS.CloudWatchLogs({ region: plugin.config.region, accessKeyId: plugin.config.accessKeyId, secretAccessKey: plugin.config.secretAccessKey })
    var paramsLog = {
      logGroupName: plugin.config.logGroupName,
      logStreamNamePrefix: plugin.config.logStreamName
    }
    cloudwatchlogs.describeLogStreams(paramsLog, function (err, data) {
      if (err) {
        plugin.logException(err)
      } else {
        if (data.logStreams === undefined || data.logStreams.length === 0) {
          plugin.logException(new Error('Log stream does not exist'))
        } else {
          let params = {
            logEvents: [
              {
                message: logs,
                timestamp: Date.now()
              }
            ],
            logGroupName: plugin.config.logGroupName,
            logStreamName: plugin.config.logStreamName,
            sequenceToken: data.logStreams[0].uploadSequenceToken
          }
          cloudwatchlogs.putLogEvents(params, function (err, data) {
            if (err) {
              plugin.logException(err)
            } else {
              plugin.log({
                title: 'Data Relayed',
                data: parsedLogs
              })
            }
          })
        }
      }
    })
  }).catch(() => {
    // console.log('--------------Catched')
  })
})

plugin.once('ready', () => {
  rkhLogger.info('AWS Cloudwatch Logger has been initialized.')
  plugin.log('AWS Cloudwatch Logger has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
